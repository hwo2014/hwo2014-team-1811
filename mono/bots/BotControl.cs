﻿using System;
using System.Collections.Generic;

namespace DickVigarista
{
    public class BotControl : BaseBot
    {
        CarId copied;

        CircularList<TickOnTrack> cTicks;

        public BotControl(string host, int port, string botName, string botKey)
            : base(host, port, botName, botKey)
        {
            cTicks = new CircularList<TickOnTrack>(new TickOnTrack.Comparer());
        }

        protected override void GameInit()
        {
            
        }

        protected override void ProcessFirstCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            foreach (var c in carPositions)
            {
                if (!c.id.Equals(myCarId))
                {
                    copied = c.id;
                }
            }
        }

        protected override void ProcessCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            Tick[] otherTicks = carsTicks[copied];
            Tick[] myTicks = carsTicks[myCarId];
            cTicks.Add(new TickOnTrack{ tick = otherTicks[0] });

            var tt = cTicks.GetNext(new TickOnTrack { tick = myTicks[0] });

            double thr = 1;
            double distance = Helper.GetTraveledDistance(myTicks[0].CarPos, tt.tick.CarPos, race);
            if (distance > 0 && MustBreak(distance, myTicks[0].dS, tt.tick.dS))
            {
                thr = 0;
            }

            SendThrottle(thr);
        }

        private bool MustBreak(double distance, double currDS, double nextDS)
        {
            double dS = currDS;
            //double distance = speed.CarPos.TrackPos - currCarPosition.TrackPos;
            //while (distance < 0)
            //{
            //    distance += race.Length[.piecePosition.lane.startLaneIndex];
            //}
            //if(distance < 0) {
            //    throw new Exception("distance should not be 0");
            //}

            dS = 0.98 * dS + 0.2 * 1;
            distance -= dS;
            dS = 0.98 * dS + 0.2 * 1;
            distance -= dS;
            while (dS > nextDS && distance > 0)
            {
                dS = 0.98 * dS;
                distance -= dS;
            }
            if (dS > nextDS)
            {
                return true;
            }

            return false;
        }
    }

    public class TickOnTrack : IComparable<TickOnTrack> {

        public Tick tick;

        public class Comparer : IComparer<TickOnTrack>{

            public int Compare(TickOnTrack x, TickOnTrack y)
            {
 	            return x.CompareTo(y);
            }
        }

        public int CompareTo(TickOnTrack other)
        {
 	        return other.tick.CarPos.TrackPos.CompareTo(other.tick.CarPos.TrackPos);
        }
    }
}
