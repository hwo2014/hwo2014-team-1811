﻿using System.Collections.Generic;

namespace DickVigarista
{
    public class BotConstant : BaseBot
    {
        public double targetDS = 1;

        public BotConstant(string host, int port, string botName, string botKey)
            : base(host, port, botName, botKey)
        {

        }

        protected override void ProcessCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            //Tick t0 = carsTicks[myCarId][0];
            //double t = 5 * (targetDS - 0.98 * t0.dS);
            //if (t > 1) t = 1;
            //if (t < 0) t = 0;

            SendThrottle(0.1);
        }
    }
}
