﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace DickVigarista
{
    /// <summary>
    /// BotSmart v2.0. Agora fazendo curvas por dentro.
    /// </summary>
    public class BotFire : BaseBot
    {
        CarPosition prevPosition;
        double speed;

        bool switchSent;

        BotFireSpec spec;
        CarPosition currPosition;

        public BotFire(string host, int port, string botName, string botKey)
            : base(host, port, botName, botKey)
        {
        }

        protected override void Init()
        {
            var specPath = LocalConfig.GetString("spec");
            if (specPath == null)
            {
                spec = new BotFireSpec
                {
                    a = 0.65,
                    b = 0.68
                };
            }
            else
            {
                try
                {
                    using (var reader = new StreamReader(specPath))
                    {
                        spec = JsonConvert.DeserializeObject<BotFireSpec>(reader.ReadToEnd());
                    }

                }
                catch
                {
                    spec = new BotFireSpec
                    {
                        a = 0,
                        b = 1
                    };
                }
            }
        }

        protected override void GameEnd()
        {
            //spec.a = (spec.a + spec.b) / 2;
            //SaveSpec(spec);
        }

        protected override void ProcessCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            if (carPositions[0].piecePosition.lap > 1 && prevPosition != currPosition)
            {
                spec.a = (spec.a + spec.b) / 2;
                Console.WriteLine(spec.a + "  "+spec.b);
                //KeepRunning = false;
                //SaveSpec(spec);
                //return;
            }

            currPosition = GetMyPosition(carPositions);

            double distanceToNextBend;
            
            /////
            SwitchInfo si = new SwitchInfo(race.track.lanes.Count);
            si.PIndex = currPosition.piecePosition.pieceIndex;
            si = switches.GetNext(si);
            int currLaneIndex = currPosition.piecePosition.lane.endLaneIndex;
            int bestLaneIndex = currLaneIndex;

            for (int i = Math.Max(currLaneIndex - 1, 0); i <= Math.Min(currLaneIndex + 1, si.LLength.Length-1); i++ )
            {
                if (si.LLength[i] < si.LLength[bestLaneIndex])
                    bestLaneIndex = i;
            }

            if (!switchSent && bestLaneIndex != currLaneIndex &&
                currPosition.piecePosition.lane.startLaneIndex == currPosition.piecePosition.lane.endLaneIndex)
            {
                SendSwitch(bestLaneIndex > currLaneIndex);
                switchSent = true;
                return;
            }

            if (prevPieceIndex != currPosition.piecePosition.pieceIndex && currPosition.Piece.@switch)
            {
                switchSent = false;
            }

            Piece nextBend = Helper.GetNextBend(race.track, currPosition, out distanceToNextBend);

            

            double dS = 0;
            if (currPosition != null)
            {
                dS = Helper.GetTraveledDistance(currPosition, prevPosition, race);
            }

            prevPosition = currPosition;
            speed = dS;

            double throttle;
            double radius = Helper.GetRadius(nextBend.Index, currPosition.piecePosition.lane.endLaneIndex, race);
            double desiredSpeed = (spec.a + spec.b) * Math.Sqrt(radius) / 2;

            //bool killHim = false;

            //var myTicks = carsTicks[myCarId];
            //foreach (var ticks in carsTicks)
            //{
            //    if (ticks.Key.Equals(myCarId)) continue;

            //    if (ticks.Value[0].StartLane ==  myTicks[0].StartLane && ticks.Value[0].EndLane == myTicks[0].EndLane)
            //    {
            //        killHim = true;
            //    }
            //}

            if (turboAvailable && !MustBreak(distanceToNextBend, dS, desiredSpeed, 3) && !currPosition.piecePosition.piece.IsBend())
            {
                SendTurbo();
                Console.WriteLine("SendTurbo");
                turboAvailable = false;
                return;
            }

            if(MustBreak(distanceToNextBend, speed, desiredSpeed)){
                throttle = 0;
            }
            else
            {
                if (race.track.pieces[currPosition.piecePosition.pieceIndex].IsBend())
                {
                    throttle = CalculateThrottle(desiredSpeed, speed);
                }
                else
                {
                    throttle = 1;
                }
                
            }

            //if (race.track.pieces[currPosition.piecePosition.pieceIndex].IsBend())
            //{
            //    // Ajustar o desiredSpeed para a curva. Livre para ser carteado.
            //    desiredSpeed = 0.4 * Math.Sqrt(Helper.GetRadius(currPosition, race));
            //    throttle = CalculateThrottle(desiredSpeed, speed);
            //}
            //else
            //{
            //    throttle = throttleLine;
            //}

            SendThrottle(throttle);
        }

        protected override void ProcessCrash(CarId carId, int gameTick)
        {
            if (!carId.Equals(myCarId)) return;
            //if (spec.crashes == null)
            //{
            //    spec.crashes = new List<BotFireSpec.Crash>();
            //}

            //spec.crashes.Add(new BotFireSpec.Crash
            //{
            //    a = spec.a,
            //    b = spec.b,
            //    lIndex = currPosition.piecePosition.lane.startLaneIndex,
            //    pIndex = currPosition.piecePosition.pieceIndex,
            //    trackname = race.track.name
            //});

            spec.b = (spec.a + spec.b) / 2;
            Console.WriteLine(spec.a + "  " + spec.b);
            //SaveSpec(spec);
            //KeepRunning = false;
        }

        protected override string GetBotName()
        {
            return botName;
            //return botName + " " + (int)(spec.a * 100) + " " + (int)(spec.b*100);
        }

        // Calculate the throttle needed to get to the desired speed given the current speed,
        // or return 0 if the throttle calculated is negative.
        private double CalculateThrottle(double desiredSpeed, double currentSpeed)
        {
            double throttle = 5 * (desiredSpeed - 0.98 * currentSpeed);
            return throttle > 0 ? (throttle < 1 ? throttle : 1) : 0;
        }

        public class BotFireSpec
        {
            public double a;
            public double b;

            public List<Crash> crashes;

            public class Crash
            {
                public string trackname;
                public int pIndex;
                public int lIndex;
                public double a;
                public double b;
            }
        }
    }
}
