﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace DickVigarista
{
    public class Bot : BaseBot
    {
        CarPosition prevPosition;

        BotSpec spec = new BotSpec
        {
            throttleLine = 0.8,
            throttleCurve = 0.5
        };

        double angle;
        double speed;

        public Bot(string host, int port, string botName, string botKey)
            : base(host, port, botName, botKey)
        {

        }

        protected override void Init()
        {
            GetSpec<BotSpec>(ref spec);
        }

        protected override void ProcessCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            CarPosition currPosition = GetMyPosition(carPositions);

            double dS = 0;
            if (prevPosition != null)
            {
                dS = Helper.GetTraveledDistance(currPosition, prevPosition, race);
            }

            

            double throttle;
            if (race.track.pieces[currPosition.piecePosition.pieceIndex].IsBend())
            {
                throttle = spec.throttleCurve;
            }
            else
            {
                throttle = spec.throttleLine;
            }

            SendThrottle(throttle);
            speed = dS;
            prevPosition = currPosition;
            angle = currPosition.angle;
        }

        public class BotSpec
        {
            public double throttleLine;
            public double throttleCurve;
        }
    }
}