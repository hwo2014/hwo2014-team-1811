﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
namespace DickVigarista
{
    public class BotGreedy : BaseBot
    {
        CarPosition prevCarPosition;
        CarPosition currCarPosition;

        double currDS;
        double prevDS;
        double alpha = 1;

        CrashInfo crashInfo;

        CircularList<DoubleOnTrack> speedOnTrack;
        DoubleOnTrack doubleOnTrack;

        public BotGreedy(string host, int port, string botName, string botKey)
            : base(host, port, botName, botKey)
        {
            speedOnTrack = new CircularList<DoubleOnTrack>(new ThrottleChangeComparer());
            doubleOnTrack = new DoubleOnTrack();
        }

        protected override void ProcessCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            currCarPosition = GetMyPosition(carPositions);
            DoubleOnTrack speed;
            if (prevCarPosition == null)
            {
                //speed = new DoubleOnTrack()
                //{
                //    Value = 1,
                //};
                //speed.Set(currCarPosition, race);
                //speedOnTrack.Add(speed);
                return;
            }

            currDS = Helper.GetTraveledDistance(currCarPosition, prevCarPosition, race);

            doubleOnTrack.Set(currCarPosition, race);
            speed = speedOnTrack.GetNext(doubleOnTrack);
            if (speed != null && MustBreak(speed))
            {
                SendThrottle(0);
            }
            else
            {
                SendThrottle(1);
            }
        }

        private bool MustBreak(DoubleOnTrack speed)
        {
            double dS = currDS;
            double distance = speed.CarPos.TrackPos - currCarPosition.TrackPos;
            while(distance < 0){
                distance += race.Length[currCarPosition.piecePosition.lane.startLaneIndex];
            }
            //if(distance < 0) {
            //    throw new Exception("distance should not be 0");
            //}

            dS = 0.98 * dS + 0.2 * 1;
            distance -= dS;
            dS = 0.98 * dS + 0.2 * 1;
            distance -= dS;
            while (dS > speed.Value && distance > 0)
            {
                dS = 0.98 * dS;
                distance -= dS;
            }
            if (dS > speed.Value)
            {
                return true;
            }

            return false;
        }

        protected override void Init()
        {
            try
            {
                using (var file = new StreamReader("botgreedy"))
                {
                    speedOnTrack.list = JsonConvert.DeserializeObject<List<DoubleOnTrack>>(file.ReadLine());
                }
            }
            catch
            {
                Console.WriteLine("no file");
            }
        }

        protected override void ProcessCrash(CarId carId, int gameTick)
        {
            Console.WriteLine("{0} : {1} : {2} : {3}", gameTick, currDS, currCarPosition.TrackPos, currCarPosition.angle);
            if (crashInfo != null)
            {
                bool consecutive = Math.Abs(currCarPosition.piecePosition.pieceIndex - crashInfo.carPos.piecePosition.pieceIndex) <= 1;
                // Crash again, but before
                if (consecutive && 
                    Helper.GetTraveledDistance(currCarPosition, crashInfo.carPos, race) < 0)
                {
                    crashInfo = GenerateCrashInfo();
                    //speedOnTrack.EraseNexts(doubleOnTrack);
                    MarkSpeed(currCarPosition);
                }
                //Crash again too close
                else if (consecutive && 
                    Helper.GetTraveledDistance(currCarPosition, crashInfo.carPos, race) < 5)
                {
                    crashInfo = GenerateCrashInfo();
                    doubleOnTrack.Set(currCarPosition, race);
                    int index = speedOnTrack.GetPrevIndex(doubleOnTrack);
                    DoubleOnTrack prev = speedOnTrack.list[index];
                    prev.Value = prev.Value - alpha;
                    Console.WriteLine("    {0} : {1}", index, prev.Value);
                    Normalize(index);
                }
                else
                {
                    crashInfo = GenerateCrashInfo();
                    MarkSpeed(currCarPosition);
                }
            }
            else
            {
                crashInfo = GenerateCrashInfo();
                speedOnTrack.EraseNexts(doubleOnTrack);
                MarkSpeed(currCarPosition);
            }

            using (var file = new StreamWriter("botgreedy"))
            {
                file.AutoFlush = true;
                file.WriteLine(JsonConvert.SerializeObject(speedOnTrack.list));
            }
            
            KeepRunning = false;
        }

        private void Normalize(int index)
        {
            if (index == 0) return;

            DoubleOnTrack prev = speedOnTrack.list[index - 1];
            DoubleOnTrack curr = speedOnTrack.list[index];

            double distance = curr.CarPos.TrackPos - prev.CarPos.TrackPos;
            double dS = prev.Value;
            double aux = 0;
            while (distance > 0 && dS > curr.Value)
            {
                distance -= dS;
                aux = dS;
                dS = 0.98 * dS;
            }

            double diff = aux - curr.Value;
            if (diff > 0)
            {
                prev.Value -= diff;
                Console.WriteLine("    {0} : {1}", index, prev.Value);
                Normalize(index - 1);
            }
            
            
        }

        private void MarkSpeed(CarPosition carPos)
        {
            DoubleOnTrack speed = new DoubleOnTrack();
            speed.Set(currCarPosition, race);
            speed.Value = currDS - alpha;
            speedOnTrack.Add(speed);
            Log.LogObject("info", new { MarkSpeed = "EraseNexts", Value = speed.Value });
        }

        CrashInfo GenerateCrashInfo()
        {
            return new CrashInfo
            {
                carPos = currCarPosition,
                dS = currDS,
            };
        }

        protected override void GameTickChange()
        {
            prevCarPosition = currCarPosition;
            prevDS = currDS;
        }
    }

    public class DoubleOnTrack : IComparable<DoubleOnTrack>
    {
        public double CompleteLength;
        public CarPosition CarPos;
        public double Value;

        public void Set(CarPosition carPos, Race race)
        {
            this.CarPos = carPos;
            int laneIndex = carPos.piecePosition.lane.startLaneIndex;
            CompleteLength = carPos.TrackPosInLane[laneIndex];
        }

        public int CompareTo(DoubleOnTrack other)
        {
            return this.CompleteLength.CompareTo(other.CompleteLength);
        }
    }

    public class ThrottleChangeComparer : IComparer<DoubleOnTrack>
    {
        public int Compare(DoubleOnTrack x, DoubleOnTrack y)
        {
            return x.CompareTo(y);
        }
    }

    public class CrashInfo
    {
        public CarPosition carPos;
        public double dS;
    }
}
