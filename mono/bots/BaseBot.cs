﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
//using System.Diagnostics;
using System.IO;
using System.Net.Sockets;

namespace DickVigarista
{
    public abstract class BaseBot
    {
        protected Race race;
        protected CarId myCarId;
        protected bool turboAvailable;
        protected int turboTicks;
        protected int turboStartTick;

        protected CircularList<SwitchInfo> switches;
        public Dictionary<CarId, Tick[]> carsTicks;
        protected int ticksLimit = 4;

        protected string host;
        protected int port;
        protected string botName;
        protected string botKey;

        protected int prevGameTick;
        protected int currentgameTick;
        protected int prevLap;
        protected int currLap;

        protected int prevPieceIndex;

        StreamWriter writer;

        protected bool KeepRunning;

        public bool GameEnded { get; private set; }

        public BaseBot(string host, int port, string botName, string botKey)
        {
            this.host = host;
            this.port = port;
            this.botName = botName;
            this.botKey = botKey;
            turboAvailable = false;
            turboTicks = 0;
            turboStartTick = 0;

            carsTicks = new Dictionary<CarId, Tick[]>();
        }

        public void Start() {
            GameEnded = false;
            KeepRunning = true;
            Init();
            using(TcpClient client = new TcpClient(host, port)) {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                if (LocalConfig.GetBool("create"))
                {
                    send(new SendCreateRace
                    {
                        botId = new BotId
                        {
                            name = GetBotName(),
                            key = botKey
                        },
                        carCount = LocalConfig.GetInt("carcount"),
                        password = LocalConfig.GetString("password"),
                        trackName = LocalConfig.GetString("trackname")
                    });
                }
                else if (!LocalConfig.GetBool("join"))
                {
                    send(new SendJoinRace(GetBotName(), botKey)
                    {
                        carCount = LocalConfig.GetInt("carcount"),
                        password = LocalConfig.GetString("password"),
                        trackName = LocalConfig.GetString("trackname")
                    });
                }
                else
                {
                    send(new SendJoin(GetBotName(), botKey));
                }

                bool firstCarPosition = true;
                //Stopwatch s = new Stopwatch();
                string line;
                while(KeepRunning)
                {
                    //s.Restart();
                    line = reader.ReadLine();
                    //s.Stop();
                    //Console.WriteLine("Server: {0}", s.ElapsedMilliseconds);
                    if (line == null) break;

                    //s.Restart();
                    var msg = JsonConvert.DeserializeObject<MsgWrapperReceive>(line);
                    Console.WriteLine(msg.gameTick);
                    Log.LogString(line);

                    currentgameTick = msg.gameTick;
                    bool pingSent = false;

                    if (msg.gameTick != prevGameTick)
                    {
                        GameTickChange();
                    }

                    if (msg.msgType == "carPositions")
                    {
                        prevLap = currLap;
                        var data = Helper.GetTypedData<List<CarPosition>>(msg.data);
                        foreach (var carPos in data)
                        {
                            carPos.Process(race);
                            ProcessTicks(carPos, firstCarPosition);
                            firstCarPosition = false;
                        }

                        CarPosition myCarPos = GetMyPosition(data);
                        currLap = myCarPos.piecePosition.lap;
                        if (msg.gameTick < 1)
                        {
                            ProcessFirstCarPositions(data, msg.gameTick);
                        }
                        else
                        {
                            ProcessCarPositions(data, msg.gameTick);
                            pingSent = true;
                        }

                        prevPieceIndex = myCarPos.piecePosition.pieceIndex;
                    }
                    else if (msg.msgType == "join")
                    {
                        var data = Helper.GetTypedData<Join>(msg.data);
                        ProcessJoin(data, msg.gameTick);
                    }
                    else if (msg.msgType == "gameInit")
                    {
                        Console.WriteLine("Race init");
                        race = Helper.GetTypedData<RaceContainer>(msg.data).race;
                        race.Process();

                        switches = GetSwitchInfo(race);
                    }
                    else if (msg.msgType == "gameEnd")
                    {
                        GameEnded = true;
                        GameEnd();
                        Console.WriteLine("Race ended");
                    }
                    else if (msg.msgType == "gameStart")
                    {
                        Console.WriteLine("Race starts");
                    }
                    else if (msg.msgType == "yourCar")
                    {
                        myCarId = Helper.GetTypedData<CarId>(msg.data);
                    }
                    else if (msg.msgType == "crash")
                    {
                        var data = Helper.GetTypedData<CarId>(msg.data);
                        ProcessCrash(data, msg.gameTick);
                    }
                    else if (msg.msgType == "turboAvailable")
                    {
                        turboAvailable = true;
                        var data = Helper.GetTypedData<TurboAvailable>(msg.data);
                        turboTicks = data.turboDurationTicks;
                        turboStartTick = msg.gameTick;
                        ProcessTurboAvailable(data, msg.gameTick);
                        pingSent = true;
                    }

                    if (msg.gameTick - turboStartTick > turboTicks)
                        turboAvailable = false;

                    if (!pingSent)
                    {
                        SendPing();
                    }

                    ShiftTicks();
                    prevGameTick = msg.gameTick;

                    //s.Stop();
                    //Console.WriteLine(s.ElapsedMilliseconds);
                }
            }
        }

        private void ShiftTicks()
        {
            foreach (var ticks in carsTicks.Values)
            {
                Tick aux = ticks[ticks.Length - 1];
                for (int i = ticks.Length - 1; i > 0; i--)
                {
                    ticks[i] = ticks[i - 1];
                }

                ticks[0] = aux;
            }
        }

        private void ProcessTicks(CarPosition carPos, bool firstCarPos)
        {
            //if (firstCarPos)
            //{
            //    Tick[] ticks = new Tick[ticksLimit];
            //    for (int i = 0; i < ticksLimit; i++)
            //    {
            //        Tick t = new Tick();
            //        ticks[i] = t;
            //        t.CarPos = carPos;
            //    }

            //    carsTicks[carPos.id] = ticks;
            //}

            //Tick t0 = carsTicks[carPos.id][0];
            //t0.gameTick = currentgameTick;

            //Tick t1 = carsTicks[carPos.id][1];

            //t0.CarPos = carPos;

            //t0.dS = Helper.GetTraveledDistance(t0.CarPos, t1.CarPos, race);
            //t0.ddS = t0.dS - t1.dS;

            //var piece = race.track.pieces[t0.CarPos.piecePosition.pieceIndex];
            //t0.A = t0.CarPos.angle * Math.PI / 180;

            //t0.RA = piece.angle * Math.PI / 180;
            //t0.RR = Helper.GetRadius(t0.CarPos.piecePosition.pieceIndex, t0.CarPos.piecePosition.lane.startLaneIndex, race);

            //t0.dA = t0.A - t1.A;
            //t0.ddA = t0.dA - t1.dA;

            //t0.StartLane = t0.CarPos.piecePosition.lane.startLaneIndex;
            //t0.EndLane = t0.CarPos.piecePosition.lane.endLaneIndex;
        }

        protected virtual void GameEnd()
        {

        }

        protected virtual void Init() { }

        /// O race foi inicializado.
        protected virtual void GameInit()
        {

        }

        protected virtual void ProcessFirstCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            SendPing();
        }

        protected virtual void ProcessCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            SendPing();
        }

        protected virtual void ProcessCrash(CarId carId, int gameTick)
        {
        }

        protected virtual void ProcessJoin(Join join, int gameTick)
        {
        }

        protected void ProcessTurboAvailable(TurboAvailable msg, int gameTick)
        {
            
        }

        protected virtual void GameTickChange()
        {
        }

        protected void SendPing()
        {
            send(new SendPing());
        }

        protected void SendThrottle(double throttle)
        {
            send(new SendThrottle(throttle));
        }

        protected void SendSwitch(bool right)
        {
            send(new SendSwitch(right));
        }

        protected void SendTurbo()
        {
            send(new SendTurbo());
        }

        protected CarPosition GetMyPosition(List<CarPosition> carPositions)
        {
            CarPosition currPosition = null;
            foreach (var carPosition in carPositions)
            {
                if (carPosition.id.Equals(myCarId))
                {
                    currPosition = carPosition;
                }
            }

            return currPosition;
        }

        private void send(SendMsg msg)
        {
            string m = msg.ToJson();
            
            writer.WriteLine(m);
            Log.LogString(m);
        }

        protected void GetSpec<T>(ref T spec)
        {
            var specPath = LocalConfig.GetString("spec");
            if (specPath == null) return;

            using (var reader = new StreamReader(specPath))
            {
                spec = JsonConvert.DeserializeObject<T>(reader.ReadToEnd());
            }
        }

        protected void SaveSpec(object spec)
        {
            var specPath = LocalConfig.GetString("spec");
            if (specPath == null) return;

            using (var reader = new StreamWriter(specPath))
            {
                reader.WriteLine(JsonConvert.SerializeObject(spec));
            }
        }

        private CircularList<SwitchInfo> GetSwitchInfo(Race race)
        {
            var switches = new CircularList<SwitchInfo>(new SwitchInfo.Comparer());
            List<Piece> uPieces = new List<Piece>();
            SwitchInfo sInfo = null;
            int piececount = race.track.pieces.Count;
            for (int pIndex = 0; pIndex < piececount; pIndex++)
            {
                Piece p = race.track.pieces[pIndex];
                if (p.@switch)
                {
                    sInfo = new SwitchInfo(p.LaneLength.Length)
                    {
                        PIndex = pIndex
                    };
                    switches.Add(sInfo);
                }
                if (sInfo == null)
                {
                    uPieces.Add(p);
                }
                else
                {
                    int lanecount = p.LaneLength.Length;
                    for (int lIndex = 0; lIndex < lanecount; lIndex++)
                    {
                        sInfo.LLength[lIndex] += p.LaneLength[lIndex];
                    }
                }
                
            }

            foreach (Piece p in uPieces)
            {
                int lanecount = p.LaneLength.Length;
                for (int lIndex = 0; lIndex < lanecount; lIndex++)
                {
                    sInfo.LLength[lIndex] += p.LaneLength[lIndex];
                }
            }

            foreach (var s in switches.list)
            {
                Console.WriteLine(s.PIndex);
                foreach (var x in s.LLength)
                {
                    Console.WriteLine("    " + x);
                }
            }

            return switches;
        }

        protected virtual string GetBotName(){
            return botName;
        }

        protected bool MustBreak(double distance, double currDS, double nextDS, double mult = 1)
        {
            double dS = currDS;

            dS = 0.98 * dS + 0.2 * mult;
            distance -= dS;
            dS = 0.98 * dS + 0.2 * mult;
            distance -= dS;
            while (dS > nextDS && distance > 0)
            {
                dS = 0.98 * dS;
                distance -= dS;
            }
            if (dS > nextDS)
            {
                return true;
            }

            return false;
        }

        //protected virtual object MetaData { get; }

        //protected abstract string BotType { get; }
    }
}
