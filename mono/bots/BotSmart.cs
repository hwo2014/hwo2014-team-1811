using System;
using System.Collections.Generic;

namespace DickVigarista
{
    /// <summary>
    /// Controla a velocidade na curva.
    /// Ajustável modificando o desiredSpeed.
    /// </summary>
    public class BotSmart : BaseBot
    {
        CarPosition prevPosition;
        BotSmartSpec spec = new BotSmartSpec
        {
            throttleLine = 1.0
        };
        
        double speed;

        public BotSmart(string host, int port, string botName, string botKey)
            : base(host, port, botName, botKey)
        {
        }

        protected override void ProcessCarPositions(List<CarPosition> carPositions, int gameTick)
        {
            CarPosition currPosition = GetMyPosition(carPositions);

            double dS = 0;
            if (currPosition != null)
            {
                dS = Helper.GetTraveledDistance(currPosition, prevPosition, race);
            }

            prevPosition = currPosition;
            speed = dS;

            double throttle;
            double desiredSpeed;
            if (race.track.pieces[currPosition.piecePosition.pieceIndex].IsBend())
            {
                // Ajustar o desiredSpeed para a curva. Livre para ser carteado.
                //desiredSpeed = 0.4 * Math.Sqrt(Helper.GetRadius(currPosition, race));
                //throttle = CalculateThrottle(desiredSpeed, speed);
            }
            else
            {
                throttle = spec.throttleLine;
            }

            SendThrottle(1);
        }

        // Calculate the throttle needed to get to the desired speed given the current speed,
        // or return 0 if the throttle calculated is negative.
        private double CalculateThrottle(double desiredSpeed, double currentSpeed)
        {
            double throttle = 5 * (desiredSpeed - 0.98 * currentSpeed);
            return throttle > 0 ? (throttle < 1 ? throttle : 1) : 0;
        }

        public class BotSmartSpec
        {
            public double throttleLine;
        }
    }
}