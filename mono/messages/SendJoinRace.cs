﻿using System;

namespace DickVigarista
{
    public class SendJoinRace : SendMsg
    {
        public BotId botId;
        public string password;
        public string trackName;
        public int carCount;

        public SendJoinRace(string name, string key)
        {
            botId = new BotId
            {
                name = name,
                key = key,
            };
            
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }
}