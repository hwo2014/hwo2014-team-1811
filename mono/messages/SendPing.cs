﻿using System;

namespace DickVigarista
{
    public class SendPing : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}