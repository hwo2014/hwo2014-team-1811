﻿using System;

namespace DickVigarista
{
    public class MsgWrapperReceive
    {
        public string msgType;
        public Object data;
        public int gameTick;
        public string gameId;
    }

    public class MsgWrapperSend
    {
        public string msgType;
        public Object data;

        public MsgWrapperSend(string msgType, object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }
}
