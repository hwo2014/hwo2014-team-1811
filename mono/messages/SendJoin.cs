﻿using System;

namespace DickVigarista
{
    public class SendJoin : SendMsg
    {
        public string name;
        public string key;

        public SendJoin(string name, string key)
        {
            this.name = name;
            this.key = key;
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
}