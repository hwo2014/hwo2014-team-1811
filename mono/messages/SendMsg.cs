﻿using Newtonsoft.Json;
using System;

namespace DickVigarista
{
    public abstract class SendMsg
    {
        private static JsonSerializerSettings sett = new JsonSerializerSettings
        {
            DefaultValueHandling = DefaultValueHandling.Ignore
        };

        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapperSend(this.MsgType(), this.MsgData()), sett);
        }

        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }
}