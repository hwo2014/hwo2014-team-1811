﻿using System;

namespace DickVigarista
{
    public class SendSwitch : SendMsg
    {
        public bool right;

        public SendSwitch(bool right)
        {
            this.right = right;
        }

        protected override Object MsgData()
        {
            if (right)
                return "Right";
            return "Left";
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}
