﻿namespace DickVigarista
{
    public class TurboAvailable
    {
        public double turboDurationMilliseconds;
        public int turboDurationTicks;
        public double turboFactor;
    }
}
