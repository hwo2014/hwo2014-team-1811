﻿using System;
using System.Collections.Generic;

namespace DickVigarista
{
    public class RaceContainer
    {
        public Race race;
    }

    public class Race
    {
        public Track track;
        public List<Car> cars;
        public RaceSession raceSession;

        public double[] Length;

        public void Process()
        {
            Length = new double[track.lanes.Count];

            int pIndex = 0;
            foreach (var p in track.pieces)
            {
                p.Index = pIndex;
                p.LaneLength = new double[Length.Length];
                p.Start = new double[Length.Length];
                for (int i = 0; i < Length.Length; i++)
                {
                    p.Start[i] = Length[i];
                    double laneLength = p.length;

                    if (p.IsBend())
                    {
                        double radius = p.radius;

                        double distCenter = track.lanes[i].distanceFromCenter;

                        if (distCenter * p.angle > 0)
                        {
                            radius -= Math.Abs(distCenter);
                        }
                        else
                        {
                            radius += Math.Abs(distCenter);
                        }
                        laneLength = Math.Abs(p.angle) / 180.0 * Math.PI * radius;
                    }

                    p.LaneLength[i] = laneLength;
                    Length[i] += laneLength;
                }

                pIndex++;
            }  
        }
    }

    public class Track
    {
        public string id;
        public string name;
        public List<Piece> pieces;
        public List<Lane> lanes;
    }

    public class Car
    {
        public CarId id;
        public CarDimensions dimensions;
    }

    public class RaceSession
    {
        public int laps;
        public int maxLapTimeMs;
        public bool quickRace;
    }

    public class Piece
    {
        public int Index;
        public double length;
        public bool @switch;
        public double radius;
        public double angle;

        public double[] Start;
        public double[] LaneLength;

        public bool IsBend()
        {
            return angle != 0.0;
        }
    }

    public class Lane
    {
        public double distanceFromCenter;
        public int index;
    }

    public class CarDimensions
    {
        public double length;
        public double guideFlagPosition;
        public double width;
    }
}