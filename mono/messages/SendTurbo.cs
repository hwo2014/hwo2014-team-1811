﻿using System;

namespace DickVigarista
{
    public class SendTurbo : SendMsg
    {
        public const string data = "Muttley, do something!";

        protected override Object MsgData()
        {
            return data;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }
}
