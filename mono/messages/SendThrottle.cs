﻿using System;

namespace DickVigarista
{
    class SendThrottle : SendMsg
    {
        public double value;

        public SendThrottle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }
}
