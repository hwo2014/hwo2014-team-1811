﻿using System;

namespace DickVigarista
{
    public class CarPosition
    {
        public double angle;
        public CarId id;
        public PiecePosition piecePosition;

        public double[] TrackPosInLane;

        public double TrackPos
        {
            get
            {
                return TrackPosInLane[piecePosition.lane.startLaneIndex];
            }
        }

        public Piece Piece
        {
            get
            {
                return piecePosition.piece;
            }
        }

        public void Process(Race race) {
            piecePosition.piece = race.track.pieces[piecePosition.pieceIndex];
            TrackPosInLane = new double[race.track.lanes.Count];
            for (int i = 0; i < race.track.lanes.Count; i++)
            {
                TrackPosInLane[i] = piecePosition.piece.Start[i] + piecePosition.inPieceDistance;
            }
        }
    }

    public class CarId
    {
        public string name;
        public string color;

        public override bool Equals(object obj)
        {
            if (obj == this) return true;
            CarId typedObj = obj as CarId;
            if (typedObj.color != this.color) return false;
            if (typedObj.name != this.name) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return (name + color).GetHashCode();
        }

        //TODO implementar GetHashCode
    }

    public class PiecePosition
    {
        public int pieceIndex;
        public double inPieceDistance;
        public int lap;

        public LaneSwitch lane;

        //Custom
        public Piece piece;
    }

    public class LaneSwitch
    {
        public int startLaneIndex;
        public int endLaneIndex;
    }
}
