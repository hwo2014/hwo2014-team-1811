﻿namespace DickVigarista
{
    public class SendCreateRace : SendMsg
    {
        public BotId botId;
        public string trackName;
        public string password;
        public int carCount;

        protected override string MsgType()
        {
            return "createRace";
        }
    }

    public class BotId
    {
        public string name;
        public string key;
    }
}
