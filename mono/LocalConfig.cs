﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace DickVigarista
{
    public static class LocalConfig
    {
        private static Dictionary<string, string> Values = new Dictionary<string, string>();

        public static void Init(string path)
        {
            StreamReader streamReader = new StreamReader(path);
            string line = streamReader.ReadToEnd();
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(line);
            foreach(var x in dict){
                Values.Add(x.Key.ToLower(), x.Value);
            }
        }

        public static string GetString(string key)
        {
            key = key.ToLower();
            string r;
            Values.TryGetValue(key, out r);
            return r;
        }

        public static bool GetBool(string key)
        {
            key = key.ToLower();
            var value = GetString(key);
            if (value == null) return false;
            return bool.Parse(value);
        }

        public static int GetInt(string key)
        {
            key = key.ToLower();
            return int.Parse(GetString(key));
        }

        public static double GetDouble(string key)
        {
            key = key.ToLower();
            return double.Parse(GetString(key));
        }
    }
}
