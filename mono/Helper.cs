﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DickVigarista
{
    public static class Helper
    {
        // Get the distance between to points in consecutive pieces
        public static double GetTraveledDistance(CarPosition current, CarPosition previous, Race race)
        {
            if (previous == null)
            {
                return current.piecePosition.inPieceDistance;
            }

            bool invert = false;
            if (current.piecePosition.pieceIndex < previous.piecePosition.pieceIndex && 
                current.piecePosition.pieceIndex != 0 && 
                previous.piecePosition.pieceIndex != race.track.pieces.Count - 1)
            {
                CarPosition aux = current;
                current = previous;
                previous = aux;
                invert = true;
            }

            double r;

            if (current.piecePosition.pieceIndex == previous.piecePosition.pieceIndex)
            {
                r = current.piecePosition.inPieceDistance - previous.piecePosition.inPieceDistance;
            }
            else
            {
                var prevPiece = race.track.pieces[previous.piecePosition.pieceIndex];
                double prevLength = prevPiece.length;

                
                if (prevLength == 0) {
                    double radius = prevPiece.radius;

                    double distCenter = race.track.lanes[previous.piecePosition.lane.startLaneIndex].distanceFromCenter;

                    if (distCenter * prevPiece.angle > 0)
                    {
                        radius -= Math.Abs(distCenter);
                    }
                    else
                    {
                        radius += Math.Abs(distCenter);
                    }
                    prevLength = Math.Abs(prevPiece.angle) / 180.0 * Math.PI * radius;
                }

                r = current.piecePosition.inPieceDistance +
                    prevLength -
                    previous.piecePosition.inPieceDistance;
            }

            return invert ? -r : r;
        }

        // Get the radius taking the lane into account
        public static double GetRadius(int pieceIndex, int laneIndex, Race race)
        {
            Piece p = race.track.pieces[pieceIndex];
            double radius = p.radius;
            if (p.length == 0)
            {
                double distCenter = race.track.lanes[laneIndex].distanceFromCenter;

                if (distCenter * p.angle > 0)
                {
                    radius -= Math.Abs(distCenter);
                }
                else
                {
                    radius += Math.Abs(distCenter);
                }
            }

            return radius;
        }

        public static T GetTypedData<T>(Object data)
        {
            string m = data.ToString();
            return JsonConvert.DeserializeObject<T>(m);
        }

        /// <summary>
        /// Retorna a próxima curva.
        /// </summary>
        /// <param name="track">a pista.</param>
        /// <param name="currPosition">posição atual.</param>
        /// <param name="distance">distância até a curva</param>
        /// <returns></returns>
        public static Piece GetNextBend(Track track, CarPosition currPosition, out double distance)
        {
            distance = track.pieces[currPosition.piecePosition.pieceIndex].LaneLength[currPosition.piecePosition.lane.startLaneIndex] - currPosition.piecePosition.inPieceDistance;
            for (int i = currPosition.piecePosition.pieceIndex + 1; i < track.pieces.Count; i++)
            {
                if (track.pieces[i].IsBend())
                    return track.pieces[i];
                distance += track.pieces[i].LaneLength[currPosition.piecePosition.lane.startLaneIndex];
            }

            for (int i = 0; i < currPosition.piecePosition.pieceIndex; i++)
            {
                if (track.pieces[i].IsBend())
                    return track.pieces[i];
                distance += track.pieces[i].LaneLength[currPosition.piecePosition.lane.startLaneIndex];
            }

            // Evitando null reference. O código nunca pode chegar aqui, a não ser q a pista não tenha curvas...
            Console.WriteLine("Deu merda aqui no GetNextBend");
            return track.pieces[currPosition.piecePosition.pieceIndex];
        }

        //public static double GetCloserDistance(CarPosition p2, CarPosition p1, Race race)
        //{
        //    double r = p2.piecePosition.piece.start + p2.piecePosition.inPieceDistance -
        //        (p1.piecePosition.piece.start + p1.piecePosition.inPieceDistance);
        //}
    }
}
