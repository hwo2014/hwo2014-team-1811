﻿using System;
using System.Collections.Generic;

namespace DickVigarista
{
    public class CircularList<T> where T : IComparable<T>
    {
        public List<T> list = new List<T>();

        IComparer<T> comparer;

        public CircularList(IComparer<T> comparer)
        {
            this.comparer = comparer;
        }

        public void GetBounds(T item, out T prevItem, out T nextItem)
        {
            prevItem = default(T);
            nextItem = default(T);
            if (list.Count == 0)
            {
                return;
            }

            if (list.Count == 1)
            {
                prevItem = list[0];
                nextItem = list[0];
            }

            int index = list.BinarySearch(item, comparer);

            int nextIndex;
            if (index < 0)
            {
                nextIndex = ~index;
            }
            else
            {
                nextIndex = index;
            }

            int prevIndex = nextIndex - 1;
            if (prevIndex < 0)
            {
                prevIndex = list.Count - 1;
            }
            if (nextIndex == list.Count)
            {
                nextIndex = 0;
            }

            prevItem = list[prevIndex];
            nextItem = list[nextIndex];
        }

        public T GetPrev(T item)
        {
            T prev;
            T next;
            GetBounds(item, out prev, out next);
            return prev;
        }

        public int GetPrevIndex(T item)
        {
            if (list.Count == 0)
            {
                return -1;
            }

            if (list.Count == 1)
            {
                return 0;
            }

            int index = list.BinarySearch(item, comparer);

            int nextIndex;
            if (index < 0)
            {
                nextIndex = ~index;
            }
            else
            {
                nextIndex = index;
            }

            int prevIndex = nextIndex - 1;
            if (prevIndex < 0)
            {
                prevIndex = list.Count - 1;
            }
            if (nextIndex == list.Count)
            {
                nextIndex = 0;
            }

            return prevIndex;
        }

        /// <summary>
        /// gets next item in circular list
        /// </summary>
        /// <param name="item">a new item that has the value you want to compare</param>
        /// <returns></returns>
        public T GetNext(T item)
        {
            T prev;
            T next;
            GetBounds(item, out prev, out next);
            return next;
        }

        public void EraseNexts(T item)
        {
            int index = list.BinarySearch(item, comparer);

            int nextIndex;
            if (index < 0)
            {
                nextIndex = ~index;
            }
            else
            {
                nextIndex = index;
            }

            list.RemoveRange(nextIndex, list.Count - nextIndex);
        }

        public void Add(T item)
        {
            // TODO considerar melhorar ordem
            list.Add(item);
            list.Sort();
        }
    }
}
