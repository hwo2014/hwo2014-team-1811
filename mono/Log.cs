﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace DickVigarista
{
    public static class Log
    {
        static StreamWriter file;
        static bool toStandard;

        public static void Init(string filePath, bool toStandard = true) {
            Log.toStandard = toStandard;
            if (!string.IsNullOrEmpty(filePath))
            {
                file = new StreamWriter(filePath);
                file.AutoFlush = true;
            }
        }

        public static void LogString(string m)
        {
            if (file == null) return;

            file.WriteLine(m);

            if (toStandard)
            {
                Console.WriteLine(m);
            }
        }

        public static void LogObject(object obj) {
            if (file == null) return;

            string m = JsonConvert.SerializeObject(obj);
            file.WriteLine(m);
            
            if(toStandard){
                Console.WriteLine(m);
            }
        }

        public static void LogObject(string level, object message)
        {
            if (file == null) return;

            string m = JsonConvert.SerializeObject(new LogItem
            {
                Level = level,
                Message = message,
            });

            file.WriteLine(m);

            if (toStandard)
            {
                Console.WriteLine(m);
            }
        }

        public static T GetTypedData<T>(Object data)
        {
            string m = data.ToString();
            return JsonConvert.DeserializeObject<T>(m);
        }
    }
}