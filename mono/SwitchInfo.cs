﻿using System;
using System.Collections.Generic;
namespace DickVigarista
{
    public class SwitchInfo : IComparable<SwitchInfo>
    {
        /// <summary>
        /// índice da peça onde o switch começa.
        /// </summary>
        public int PIndex;

        /// <summary>
        /// distância até o próximo switch pra cada lane até o próximo switch.
        /// </summary>
        public double[] LLength;

        /// <summary>
        /// Creates a new SwitchInfo
        /// </summary>
        /// <param name="laneCount">Quantidade de lanes.</param>
        public SwitchInfo(int laneCount)
        {
            LLength = new double[laneCount];
        }

        public class Comparer : IComparer<SwitchInfo>
        {
            public int Compare(SwitchInfo x, SwitchInfo y)
            {
                return x.PIndex.CompareTo(y.PIndex);
            }
        }

        public int CompareTo(SwitchInfo other)
        {
            return this.PIndex.CompareTo(other.PIndex);
        }
    }
}
