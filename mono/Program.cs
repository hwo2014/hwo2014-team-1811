﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading;

namespace DickVigarista
{
    class Program
    {
        static string host;
        static int port;
        static string botName;
        static string botKey;

        public static void Main(string[] args)
        {
            host = args[0];
            port = int.Parse(args[1]);
            botName = args[2];
            botKey = args[3];

            if (args.Length < 5)
            {
                LocalConfig.Init("Config.json");
            }
            else
            {
                string configPath = args[4];
                LocalConfig.Init(configPath);
            }

            botName = LocalConfig.GetString("botname");

            string fileName = LocalConfig.GetString("logPath") + "logs_" + botName + "_" + DateTime.Now.ToString("o").Replace(":", "") + ".txt";
            Console.WriteLine("Logfile: {0}", fileName);
            Log.Init(fileName, LocalConfig.GetBool("toStandard"));

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            string mode = LocalConfig.GetString("mode");
            int times;

            if (LocalConfig.GetString("times") == null)
            {
                times = 1;
            }else {
                times = LocalConfig.GetInt("times");
            }

            for (int i = 0; i < times; i++)
            {
                if (mode == null)
                {
                    // Isso eh o que vai rodar nos automated tests. A gente coloca aqui o nosso bot mais eficiente. Os outros blocos de if
                    // servem pra outros testes.
                    BaseBot bot = new BotFire(host, port, botName, botKey);
                    bot.Start();
                }
                else if (mode == "botgreedytests")
                {
                    BaseBot bot = new BotGreedy(host, port, botName, botKey);
                    while (!bot.GameEnded)
                    {
                        bot.Start();
                    }
                }
                else if (mode == "bot")
                {
                    BaseBot bot = new Bot(host, port, botName, botKey);
                    bot.Start();
                }
                else if (mode == "botfire")
                {
                    BaseBot bot = new BotFire(host, port, botName, botKey);
                    bot.Start();
                }
                else if (mode == "botsmart")
                {
                    BaseBot bot = new BotSmart(host, port, botName, botKey);
                    bot.Start();
                }
                else if (mode == "botconstant")
                {
                    BaseBot bot = new BotConstant(host, port, botName, botKey);
                    bot.Start();
                }
                else if (mode == "botcontrol")
                {
                    BaseBot bot = new BotControl(host, port, botName, botKey);
                    bot.Start();
                }
                else
                {
                    Console.WriteLine("Mode '{0}' not defined", mode);
                }
            }
        }
    }
}