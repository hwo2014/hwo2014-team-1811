﻿namespace DickVigarista
{
    public class Tick
    {
        public double RA { get; set; }
        public double RR { get; set; }
        public double A { get; set; }
        public double dA { get; set; }
        public double ddA { get; set; }
        public double T { get; set; }
        //public double L { get; set; }
        //public double W { get; set; }
        public CarPosition CarPos;
        /// <summary>
        /// Distance between the front of the car and the guide flag.
        /// </summary>
        //public double F { get; set; }

        public Tick pPt { get; set; }

        public double dS { get; set; }
        public double ddS { get; set; }

        public int gameTick { get; set; }
        public int StartLane;
        public int EndLane;
        //public int pieceIndex { get; set; }
    }
}
