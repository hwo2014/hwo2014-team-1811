﻿using DickVigarista;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ThrottleAnalysis
{
    public class TestAngle
    {
        public Race race;
        public CarPosition prevCarPos;
        public MsgWrapperReceive prevMsg;

        public RegressionPoint prevRegPt;
        public List<RegressionPoint> regPts = new List<RegressionPoint>();

        public int gameTick;
        public bool crashed;
        public bool stop;

        public int group;

        public void Run()
        {
            StreamReader reader = new StreamReader(LocalConfig.GetString("LogPath"));
            
            string line;
            //Log.Init(LocalConfig.GetString("toLog"));
            Cabecalho();

            try
            {
                while ((line = reader.ReadLine()) != null)
                {
                    var msg = JsonConvert.DeserializeObject<MsgWrapperReceive>(line);
                    if (msg.msgType == "carPositions")
                    {
                        var data = Helper.GetTypedData<List<CarPosition>>(msg.data);
                        foreach(var carPos in data) {
                            carPos.Process(race);
                        }
                        Process(reader, msg);
                    }
                    else if (msg.msgType == "gameInit")
                    {
                        Console.WriteLine("Race init");
                        race = Helper.GetTypedData<RaceContainer>(msg.data).race;
                        race.Process();
                    }
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Build();
        }

        public void Process(StreamReader reader, MsgWrapperReceive msg)
        {
            if (msg.gameTick < 10)
            {
                return;
            }

            var carPos = Log.GetTypedData<List<CarPosition>>(msg.data)[0];

            string line = reader.ReadLine();
            var throtleLog = JsonConvert.DeserializeObject<ThrottleLog>(line);

            var regPt = new RegressionPoint()
            {
                dS = Helper.GetTraveledDistance(carPos, prevCarPos, race),
                pPt = prevRegPt,
                gameTick = msg.gameTick,
                pieceIndex = carPos.piecePosition.pieceIndex
            };

            crashed = regPt.dS == 0;

            var piece = race.track.pieces[carPos.piecePosition.pieceIndex];
            regPt.CarAngle = carPos.angle * Math.PI / 180;
            
            regPt.RoadAngle = piece.angle * Math.PI / 180;
            //regPt.RoadRadius = Helper.GetRadius(carPos, race);
            regPt.Throttle = throtleLog.data;
            regPt.CarWidth = race.cars[0].dimensions.width;
            regPt.CarLength = race.cars[0].dimensions.length;
            regPt.CarGuideFlag = race.cars[0].dimensions.guideFlagPosition;
            regPt.piecePos = carPos.piecePosition.inPieceDistance;

            if (prevRegPt != null)
            {
                regPt.dAngle = regPt.CarAngle - regPt.pPt.CarAngle;
            }

            if (prevRegPt != null && prevRegPt.pPt != null)
            {
                regPt.ddAngle = regPt.dAngle - regPt.pPt.dAngle;
                regPt.ddS = regPt.dS - regPt.pPt.dS;

                if (regPt.gameTick >= 87 && regPt.gameTick <= 128)
                {
                    Print(regPt);
                    regPts.Add(regPt);
                }

                //if (regPt.pPt.pPt.RoadAngle == 0 && regPt.RoadAngle != 0)
                //{
                //    Print(regPt);
                //    regPts.Add(regPt);
                //}

                //Print(regPt);
            }

            prevMsg = msg;
            prevCarPos = carPos;
            prevRegPt = regPt;
        }

        private void Cabecalho()
        {
            Log.LogString(
                "gameTick,"+
                //"pieceIndex,"+
                "RoadAngle,"+
                "RoadRadius,"+
                "dS," +
                "piecePos," +
                "dAngle," +
                "CarAngle,"+
                //"Throttle,"+
                "CarLength,"+
                //"CarWidth,"+
                "CarGuideFlag");
        }

        public void Print(RegressionPoint regPt)
        {
            var sb = new StringBuilder();

            var v = new [] {
                regPt.gameTick,
                //regPt.pieceIndex,
                regPt.RoadAngle,
                regPt.RoadRadius,
                regPt.dS,
                regPt.piecePos,
                regPt.dAngle,
                regPt.CarAngle,
                //regPt.Throttle,
                regPt.CarLength,
                //regPt.CarWidth,
                regPt.CarGuideFlag
            };

            for (int i = 0; i < v.Length; i++)
            {
                sb.Append(v[i]);
                if (i < v.Length - 1)
                {
                    sb.Append(",");
                }
            }

            Log.LogString(sb.ToString());
        }

        public void Build()
        {
            double[,] xy = new double[regPts.Count, 20];

            //double error = 0;
            int j = 0;
            for (int i = 0; i < regPts.Count; i++)
            {
                var regPt = regPts[i];
                var pPt = regPt.pPt;
                j = 0;

                //double g = pPt.CarGuideFlag;
                //double l = pPt.CarLength;
                //double w = pPt.CarWidth;
                //double cos = Math.Cos(pPt.dAngle);
                //double cos2 = cos*cos;
                //double sin2a = Math.Sin(pPt.CarAngle*2);

                //double d = pPt.CarLength / 2 - pPt.CarGuideFlag;
                //double K1 = P2(pPt.dS) * Math.Cos(pPt.dAngle) / pPt.CCRadius;
                //double K2 = d * d * pPt.dAngle * (cos2 - sin2a / 2);
                //double K3 = g * g - g * l + l * l / 3 + w * w / 12 - d * d * sin2a / 2;

                double alpha = D(regPt.dS, regPt.RoadRadius);
                double beta = (Math.PI - alpha) / 2;

                double acc = regPt.dS * Math.Sin(alpha/2);

                //xy[i, j++] = acc * Math.Sin(Math.PI - beta - pPt.CarAngle);

                xy[i, j++] = Acc(regPt);
                xy[i, j++] = Acc(pPt);
                xy[i, j++] = Acc(pPt.pPt);

                //xy[i, j++] = D(regPt.dS, regPt.RoadRadius);
                //xy[i, j++] = pPt.pPt.dAngle;
                xy[i, j++] = pPt.dAngle;

                xy[i, j++] = regPt.ddAngle;
            }

            int info;
            alglib.linearmodel lm;
            alglib.lrreport ar;
            
            alglib.lrbuildz(xy, regPts.Count, j - 1, out info, out lm, out ar);
            double avgrelerror = ar.avgrelerror;
            double rmserror = ar.rmserror;

        }

        private static double Acc(RegressionPoint Pt)
        {
            double alpha = D(Pt.dS, Pt.RoadRadius);
            double beta = (Math.PI - alpha) / 2;

            double acc = Pt.dS * Math.Sin(alpha / 2);
            return acc * Math.Sin(Math.PI - beta - Pt.pPt.CarAngle);
        }

        private static double D(double a, double b){
            if(b == 0) return 0;
            return a / b;
        }

        private static double P2(double a)
        {
            return a * a;
        }
    }
}
