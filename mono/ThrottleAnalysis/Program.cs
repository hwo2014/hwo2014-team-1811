﻿using System;
using System.Collections.Generic;
using System.IO;
using DickVigarista;
using Newtonsoft.Json;

namespace ThrottleAnalysis
{
    class Program
    {
        static void Main(string[] args)
        {
            LocalConfig.Init("LocalConfig.json");
            //Log.Init(LocalConfig.GetString("toLog"), LocalConfig.GetBool("toStandard"));
            string mode = LocalConfig.GetString("mode");
            if(mode == "TestCrash") {
                TestCrash testCrash = new TestCrash();
                testCrash.Run();
                return;
            }
            if (mode == "TestAngle")
            {
                TestAngle mytest = new TestAngle();
                mytest.Run();
                return;
            }

            TestDerrapagem test = new TestDerrapagem();

            StreamReader reader = new StreamReader(LocalConfig.GetString("LogPath"));
            string line;
            bool gameStart = false;
            try
            {
                while ((line = reader.ReadLine()) != null && !test.stop)
                {
                    var msg = JsonConvert.DeserializeObject<MsgWrapperReceive>(line);
                    switch (msg.msgType)
                    {
                        case "carPositions":
                            if (!gameStart) break;
                            test.Process(reader, msg);
                            break;
                        case "gameInit":
                            test.race = Log.GetTypedData<RaceContainer>(msg.data).race;
                            break;
                        case "gameStart":
                            gameStart = true;
                            break;
                        case "crash":
                            test.crashed = true;
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            test.Run();
        }
    }
}
