﻿using System;

namespace ThrottleAnalysis
{
    public static class RegHelper
    {
        public static void GetLine(double[,] xy, int i, double[] r)
        {
            for (int k = 0; k < r.Length; k++)
            {
                r[k] = xy[i, k];
            }
        }

        /// <summary>
        /// Calcula a aceleração centrípeta no guide flag.
        /// </summary>
        /// <param name="regPt"></param>
        /// <returns></returns>
        public static double F(RegressionPoint regPt)
        {
            return regPt.RoadRadius == 0 ? 0 : regPt.dS * regPt.dS / regPt.RoadRadius;
        }

        /// <summary>
        /// Calcula a aceleração centrífuga no centro do carro.
        /// </summary>
        /// <param name="regPt"></param>
        /// <returns></returns>
        public static double CentF(RegressionPoint regPt)
        {
            return regPt.RoadRadius ==
                0 ? 0 : regPt.dS * regPt.dS /
                        (regPt.RoadRadius + (regPt.CarLength / 2 - regPt.CarGuideFlag) * Math.Sin(regPt.CarAngle));
        }

        /// <summary>
        /// Calcula a aceleração resultante da centrífuga e da angular no centro do carro.
        /// </summary>
        /// <param name="regPt"></param>
        /// <returns></returns>
        public static double ResF(RegressionPoint regPt)
        {
            if (regPt.RoadRadius == 0)
                return 0;

            if (regPt.pPt == null || regPt.pPt.pPt == null)
                return CentF(regPt);

            // distância entre o centro e o guide flag
            double d = regPt.CarLength / 2 - regPt.CarGuideFlag;

            // velocidade angular
            double omega = regPt.CarAngle - regPt.pPt.CarAngle;

            // aceleração angular
            double alpha = regPt.CarAngle - 2 * regPt.pPt.CarAngle + regPt.pPt.pPt.CarAngle;

            return CentF(regPt) + d * alpha * Math.Sin(regPt.CarAngle) + d * omega * (Math.Cos(regPt.CarAngle) - Math.Sin(regPt.CarAngle));
        }
    }
}
