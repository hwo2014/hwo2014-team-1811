﻿using DickVigarista;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace ThrottleAnalysis
{
    public class TestDerrapagem
    {
        public Race race;
        public CarPosition prevCarPos;
        public MsgWrapperReceive prevMsg;

        public RegressionPoint prevRegPt;
        public List<RegressionPoint> regPts = new List<RegressionPoint>();

        public int gameTick;
        public bool crashed;
        public bool stop;

        public int group;

        public void Run()
        {
            LocalConfig.Init("LocalConfig.json");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int nPts = regPts.Count;

            string fileName = LocalConfig.GetString("toLog") + "logs_" +
                              DateTime.Now.ToString("o").Replace(":", "") + ".txt";
            Console.WriteLine("Logfile: {0}", fileName);
            Log.Init(fileName, LocalConfig.GetBool("toStandard"));

            double[,] xy = new double[regPts.Count, 20];

            double error = 0;
            int j = 0;
            for (int i = 0; i < nPts; i++)
            {
                var regPt = regPts[i];
                var prevRegPt = regPt.pPt;

                // momento de inércia
                double inertia = regPt.CarGuideFlag * (regPt.CarGuideFlag - regPt.CarLength) + regPt.CarLength * regPt.CarLength / 3 + regPt.CarWidth / 12;

                // distância entre o centro e o guide flag
                double d = regPt.CarLength / 2 - regPt.CarGuideFlag;
                double denominador = inertia - d * d * Math.Sin(prevRegPt.CarAngle) * Math.Cos(prevRegPt.CarAngle);
                double numerador = d * d * Math.Cos(regPt.CarAngle) * (Math.Cos(regPt.CarAngle) - Math.Sin(regPt.CarAngle));

                j = 0;
                //xy[i, j++] = RegHelper.CentF(prevRegPt) * Math.Cos(prevRegPt.CarAngle);
                //xy[i, j++] = (0.2 * regPt.Throttle - 0.02 * prevRegPt.dS) * Math.Sin(regPt.CarAngle);
                xy[i, j++] = prevRegPt.CarAngle - prevRegPt.pPt.CarAngle;
                //xy[i, j++] = prevRegPt.PrevPt.CarAngle;
                xy[i, j++] = prevRegPt.pPt.CarAngle - prevRegPt.pPt.pPt.CarAngle;
                //xy[i, j++] = d * RegHelper.CentF(prevRegPt) * Math.Cos(prevRegPt.CarAngle) / denominador;
                //xy[i, j++] = RegHelper.ResF(regPt) * Math.Cos(regPt.CarAngle) / denominador;
                //xy[i, j++] = d * d * Math.Cos(prevRegPt.CarAngle) * (prevRegPt.CarAngle - prevRegPt.PrevPt.CarAngle) * (Math.Cos(prevRegPt.CarAngle) - Math.Sin(prevRegPt.CarAngle)) /
                  //              denominador;
                //xy[i, j++] = prevRegPt.CarAngle - prevRegPt.PrevPt.CarAngle;
                //xy[i, j++] = (prevRegPt.dS - prevRegPt.PrevPt.dS) / regPt.RoadRadius == 0 ? double.MaxValue : regPt.RoadRadius;
                //xy[i, j++] = (prevRegPt.dS - prevRegPt.PrevPt.dS) * Math.Sin(prevRegPt.CarAngle);

                xy[i, j++] = regPt.CarAngle - prevRegPt.CarAngle;
            }

            int info;
            alglib.linearmodel lm;
            alglib.lrreport ar;
            alglib.lrbuildz(xy, nPts, j - 1, out info, out lm, out ar);

            double relrmserror = ar.rmserror * ar.avgrelerror / (ar.avgerror);

            Log.LogString(lm.ToString());

            error = 0;

            double[] r = new double[j];
            for (int i = 0; i < nPts; i++)
            {
                var regPt = regPts[i];
                double actual = regPt.CarAngle - regPt.pPt.CarAngle;
                RegHelper.GetLine(xy, i, r);
                double expected = alglib.lrprocess(lm, r);
                double derror = actual - expected;

                if (i < 100)
                {
                    Console.WriteLine("{0} : {1} : {2} : {3} : {4}", i,
                                                                     regPt.gameTick,
                                                                     expected,
                                                                     actual,
                                                                     regPt.pPt.CarAngle);
                }

                error += derror * derror;
            }

            error = Math.Sqrt(error) / (nPts - 1);

            Console.WriteLine("Error reg: {0}", error);

            stopwatch.Stop();
            Console.WriteLine("RL: {0} ms", stopwatch.ElapsedMilliseconds);
        }

        public void Process(StreamReader reader, MsgWrapperReceive msg)
        {
            gameTick = msg.gameTick;
            var carPos = Log.GetTypedData<List<CarPosition>>(msg.data)[0];

            string line = reader.ReadLine();
            var throtleLog = JsonConvert.DeserializeObject<ThrottleLog>(line);

            var regPt = new RegressionPoint()
            {
                dS = Helper.GetTraveledDistance(carPos, prevCarPos, race),
                pPt = prevRegPt,
                gameTick = msg.gameTick,
                pieceIndex = carPos.piecePosition.pieceIndex
            };

            crashed = regPt.dS == 0;

            var piece = race.track.pieces[carPos.piecePosition.pieceIndex];
            regPt.CarAngle = carPos.angle * Math.PI / 180;
            regPt.RoadAngle = piece.angle * Math.PI / 180;
            //regPt.RoadRadius = Helper.GetRadius(carPos, race);
            regPt.Throttle = throtleLog.data;
            regPt.CarWidth = race.cars[0].dimensions.width;
            regPt.CarLength = race.cars[0].dimensions.length;
            regPt.CarGuideFlag = race.cars[0].dimensions.guideFlagPosition;


            if (prevRegPt != null && prevRegPt.pPt != null && prevRegPt.pPt.pPt != null)
            {
                if (regPt.RoadRadius != 0 && !crashed)
                {
                    regPts.Add(regPt);
                }
            }

            prevMsg = msg;
            prevCarPos = carPos;
            prevRegPt = regPt;
        }
    }
}
