﻿using DickVigarista;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ThrottleAnalysis
{
    public class RegressionPoint
    {
        public double RoadAngle { get; set; }
        public double RoadRadius { get; set; }
        public double CarAngle { get; set; }
        public double dAngle { get; set; }
        public double ddAngle { get; set; }
        public double Throttle { get; set; }
        public double CarLength { get; set; }
        public double CarWidth { get; set; }
        public double piecePos;

        /// <summary>
        /// Distance between the front of the car and the guide flag.
        /// </summary>
        public double CarGuideFlag { get; set; }

        public RegressionPoint pPt { get; set; }

        public double dS { get; set; }
        public double ddS { get; set; }

        public int gameTick { get; set; }
        public int pieceIndex { get; set; }

        //Car center radius
        public double CCRadius
        {
            get {
                return RoadRadius + (CarLength / 2 - CarGuideFlag) * Math.Sin(CarAngle);
            }
        }
    }

    public class RegressionTest
    {
        public List<RegressionPoint> regPts;

        //Deslocamento
        public void Run()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int nPts = regPts.Count;
            int nVar = 3;

            double[,] xy = new double[regPts.Count, nVar + 1];

            double error = 0;
            for (int i = 1; i < nPts; i++)
            {
                var regPt = regPts[i];
                int j = 0;
                xy[i, j++] = regPt.pPt.dS;
                xy[i, j++] = regPt.pPt.Throttle;

                xy[i, j++] = regPt.dS;

                double derror = regPt.dS - (regPt.pPt.dS * 0.98 + regPt.pPt.Throttle * 0.2);
                Console.WriteLine(derror);
                error += derror * derror;
            }

            error = Math.Sqrt(error) / (nPts - 1);
            Console.WriteLine("Error pada: {0}", error);


            int info;
            alglib.linearmodel lm;
            alglib.lrreport ar;
            alglib.lrbuildz(xy, nPts, nVar, out info, out lm, out ar);

            //error = 0;
            //for (int i = 0; i < nPts; i++)
            //{
            //    var regPt = regPts[i];
            //    double derror = regPt.dS - (regPt.PrevPt.dS * lm.innerobj.w[4] + regPt.PrevPt.Throttle * lm.innerobj.w[5] + lm.innerobj.w[6]);
            //    Console.WriteLine(derror);
            //    error += derror * derror;
            //}

            //error = Math.Sqrt(error) / (nPts-1);
            //Console.WriteLine("Error reg: {0}", error);

            stopwatch.Stop();
            Console.WriteLine("RL: {0} ms", stopwatch.ElapsedMilliseconds);
        }
    }
}
