﻿using DickVigarista;
using Newtonsoft.Json;
using System;
using System.IO;
namespace ThrottleAnalysis
{
    public class TestCrash
    {
        public void Run()
        {
            StreamReader reader = new StreamReader(LocalConfig.GetString("LogPath"));
            string line;
            
            try
            {
                while ((line = reader.ReadLine()) != null)
                {
                    var msg = JsonConvert.DeserializeObject<MsgWrapperReceive>(line);
                    if (msg.msgType == "crash")
                    {
                        
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
