﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThrottleAnalysis
{
    class alglibtest
    {
        static void Test(string[] args)
        {
            alglib alglib = new alglib();

            double[,] X = new double[4 * 4 * 4, 3];
            int c = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        X[c, 0] = i;
                        X[c, 1] = j;
                        X[c, 2] = k;

                        c++;
                    }
                }
            }

            double[] P = new double[] { 4, 1, 2, 5 };

            double[,] xy = new double[X.GetLength(0), X.GetLength(1) + 1];

            for (int i = 0; i < X.GetLength(0); i++)
            {
                double y = P[0];
                for (int j = 1; j < P.Length; j++)
                {
                    y += X[i, j - 1] * P[j];
                    xy[i, j - 1] = X[i, j - 1];
                }
                xy[i, X.GetLength(1)] = y;
            }

            int info;
            alglib.linearmodel lm;
            alglib.lrreport ar;
            alglib.lrbuild(xy, X.GetLength(0), X.GetLength(1), out info, out lm, out ar);

            var w = lm.innerobj.w;


            //Console.WriteLine();
            Console.WriteLine(w[0]);

            Console.WriteLine("finish");
        }
    }
}
