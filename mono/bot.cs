using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace DickVigarista
{
    class Bot
    {
        Race race;
        CarPosition prevPosition;
        CarId myCarId;
        double throttle = 0.8;
        double angle;
        double speed;

        public static void Main(string[] args) {
            LocalConfig.Init("LocalConfig.json");
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            string fileName = "Logs/logs_" + botName + "_" +
                                 DateTime.Now.ToString("o").Replace(":", "") + ".txt";
            Console.WriteLine("Logfile: {0}", fileName);
            Log.Init(fileName, LocalConfig.GetBool("toStandard"));

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using(TcpClient client = new TcpClient(host, port)) {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                new Bot(reader, writer, new Join(botName, botKey));
            }
        }

        private StreamWriter writer;

        Bot(StreamReader reader, StreamWriter writer, Join join) {
            this.writer = writer;
            string line;

            send(join);

            while((line = reader.ReadLine()) != null) {
                var msg = JsonConvert.DeserializeObject<MsgWrapperReceive>(line);
                /*if (msg.gameTick > 1000)
                {
                    break;
                }
                if (msg.msgType == "crash")
                {
                    break;
                }*/
                Log.LogString(line);
                switch(msg.msgType) {
                    case "carPositions":
                        if (GameLoop(msg))
                        {
                        }
                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        send(new Ping());
                        break;
                    case "gameInit":
                        Console.WriteLine("Race init");
                        race = GetTypedData<RaceContainer>(msg.data).race;
                        send(new Ping());
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        break;
                    case "yourCar":
                        myCarId = GetTypedData<CarId>(msg.data);
                        send(new Ping());
                        break;
                    case "crash":
                        Log.LogObject(new { type = "crash", ang = angle, dS = speed});
                        //crashed = true;
                        send(new Ping());
                        break;
                    default:
                        send(new Ping());
                        break;
                }
            }
        }

        private bool GameLoop(MsgWrapperReceive msg) {
            var carPositions = GetTypedData<List<CarPosition>>(msg.data);
            CarPosition currPosition = null;
            foreach (var carPosition in carPositions)
            {
                if (carPosition.id.Equals(myCarId))
                {
                    currPosition = carPosition;
                }

                /*if (carPosition.piecePosition.lap > 1)
                {
                    return true;
                }*/
            }

            double dS = 0;
            if (currPosition != null)
            {
                dS = GetTraveledDistance(currPosition, prevPosition);
            }

            prevPosition = currPosition;

            Log.LogObject(new { dS = dS,
                                ang = currPosition.angle,
                                radius = race.track.pieces[currPosition.piecePosition.pieceIndex].radius,
                                gameTick = msg.gameTick, 
                                gameId = msg.gameId });

            if (race.track.pieces[currPosition.piecePosition.pieceIndex].IsBend())
            {
                throttle = 0.5;
            }
            else
            {
                throttle = 0.8;
            }

            /*if (currPosition.angle > 30.0 || currPosition.angle < -30.0)
            {
                //Console.WriteLine(currPosition.angle);
                throttle /= 2;
            }*/

            send(new Throttle(throttle));
            speed = dS;
            angle = currPosition.angle;

            return false;
        }

        private void send(SendMsg msg) {
            string m = msg.ToJson();
            
            writer.WriteLine(m);
            Log.LogString(m);
        }

        private T GetTypedData<T>(Object data)
        {
            string m = data.ToString();
            return JsonConvert.DeserializeObject<T>(m);
        }

        private double GetTraveledDistance(CarPosition current, CarPosition previous)
        {
            if (previous == null)
            {
                return current.piecePosition.inPieceDistance;
            }

            if (current.piecePosition.pieceIndex == previous.piecePosition.pieceIndex)
            {
                return current.piecePosition.inPieceDistance - previous.piecePosition.inPieceDistance;
            }
            else
            {
                return current.piecePosition.inPieceDistance + 
                    race.track.pieces[previous.piecePosition.pieceIndex].length - 
                    previous.piecePosition.inPieceDistance;
            }
        }
    }
}