﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DickVigarista
{
    [TestClass]
    public class CircularListTest
    {
        [TestMethod]
        public void CircularListTest_SimpleTest()
        {
            CircularList<double> cl = new CircularList<double>(new DoubleComparer());

            cl.Add(1);
            cl.Add(2);
            cl.Add(3);
            cl.Add(4);

            double a1;
            double a2;
            cl.GetBounds(1.5, out a1, out a2);
            Assert.AreEqual(1, a1);
            Assert.AreEqual(2, a2);

            cl.GetBounds(2.3, out a1, out a2);
            Assert.AreEqual(2, a1);
            Assert.AreEqual(3, a2);

            cl.GetBounds(3.2, out a1, out a2);
            Assert.AreEqual(3, a1);
            Assert.AreEqual(4, a2);

            cl.GetBounds(4.2, out a1, out a2);
            Assert.AreEqual(4, a1);
            Assert.AreEqual(1, a2);
        }
    }

    class DoubleComparer : IComparer<double>
    {
        public int Compare(double x, double y)
        {
            return x.CompareTo(y);
        }
    }
}
